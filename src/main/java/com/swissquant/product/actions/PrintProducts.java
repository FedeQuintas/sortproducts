package com.swissquant.product.actions;

import com.swissquant.product.domain.Product;

import java.util.List;

public class PrintProducts {

    public void print(int amountToPrint, List<Product> products) {
        amountToPrint = products.size() < amountToPrint ? products.size() : amountToPrint;
        products = products.subList(0, amountToPrint);
        products.stream().forEach((product) -> System.out.println(product.toString()));
    }
}
