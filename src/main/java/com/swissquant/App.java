package com.swissquant;

import com.swissquant.product.actions.ImportProducts;
import com.swissquant.product.actions.PrintProducts;
import com.swissquant.product.actions.SortProducts;

public class App
{
    public static void main( String[] args )
    {
        new SortProductsApplication(new SortProducts(), new PrintProducts(), new ImportProducts()).run(args);
    }
}
