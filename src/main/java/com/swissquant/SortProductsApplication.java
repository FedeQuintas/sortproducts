package com.swissquant;

import com.swissquant.product.actions.ImportProducts;
import com.swissquant.product.actions.PrintProducts;
import com.swissquant.product.actions.SortProducts;
import com.swissquant.product.domain.Product;

import java.util.List;

public class SortProductsApplication {

    private ImportProducts importProducts;
    private PrintProducts printProducts;
    private SortProducts sortProducts;

    public SortProductsApplication(SortProducts sortProducts, PrintProducts printProducts, ImportProducts importProducts) {
        this.sortProducts = sortProducts;
        this.printProducts = printProducts;
        this.importProducts = importProducts;
    }

    public void run(String[] args) {

        List<Product> products = importProducts.importFromCsv(args);

        sortProducts.sort(products);

        printProducts.print(10, products);
    }
}
