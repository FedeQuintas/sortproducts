package com.swissquant.product.domain;

import java.math.BigDecimal;

public class ProductFactory {

    BigDecimal currencyExchange = new BigDecimal(1.14);

    public Product createProduct(BigDecimal price, String date, Integer quantity, String description, String currency) {
        BigDecimal value = price;
        if(currency.equals("EUR")){
            value = price.multiply(currencyExchange);
        }
        return new Product(value, date, quantity, description);
    }
}
