package com.swissquant.product.actions;

import com.swissquant.product.domain.Product;

import java.util.Collections;
import java.util.List;

public class SortProducts {

    public void sort(List<Product> products) {
        Collections.sort(products, (Product o1, Product o2) -> {
            if(o1 instanceof Product && o2 instanceof Product)
            {
                return o1.compareTo(o2);
            }
            return 0;
        });
    }
}
