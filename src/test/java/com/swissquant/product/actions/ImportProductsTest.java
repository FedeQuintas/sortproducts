package com.swissquant.product.actions;

import com.swissquant.product.domain.Product;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ImportProductsTest {

    @Test
    public void whenImportProductsFromOneFileThenTheyAreRetrievedWithAllTheFields()
    {

        String[] files = {"src/test/resources/products1-test.csv"};

        List<Product> products = new ImportProducts().importFromCsv(files);

        Product firstProduct = products.get(0);

        Assert.assertEquals(4, products.size());
        Assert.assertEquals(new BigDecimal("40.00"), firstProduct.getValue());
        Assert.assertEquals(1000, firstProduct.getQuantity());
        Assert.assertEquals("1/1/2008", firstProduct.getDate());
        Assert.assertEquals("Blue Shoes", firstProduct.getDescription());
    }

    @Test
    public void whenImportProductsFromTwoFilesThenTheyAreRetrievedWithAllTheFields()
    {

        String[] files = {"src/test/resources/products1-test.csv", "src/test/resources/products2-test.csv"};

        List<Product> products = new ImportProducts().importFromCsv(files);

        Product firstProductFromSecondFile = products.get(4);

        Assert.assertEquals(8, products.size());
        Assert.assertEquals(new BigDecimal("10.00"), firstProductFromSecondFile.getValue());
        Assert.assertEquals(2000, firstProductFromSecondFile.getQuantity());
        Assert.assertEquals("1/1/2009", firstProductFromSecondFile.getDate());
        Assert.assertEquals("Brown Shoes", firstProductFromSecondFile.getDescription());
    }

    @Test
    public void whenImportProductsWithPriceInEurosThenTheyAreRetrievedWithTheValueInCHF()
    {

        String[] files = {"src/test/resources/products1-test.csv"};

        List<Product> products = new ImportProducts().importFromCsv(files);

        Product product = products.get(2);

        Assert.assertEquals(new BigDecimal("22.799999999999998046007476659724488854408264160156250000"), product.getValue());
    }
}
