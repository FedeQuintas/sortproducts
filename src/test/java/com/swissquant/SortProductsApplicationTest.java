package com.swissquant;

import com.swissquant.product.actions.ImportProducts;
import com.swissquant.product.actions.PrintProducts;
import com.swissquant.product.actions.SortProducts;
import com.swissquant.product.domain.Product;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SortProductsApplicationTest {

    SortProducts sortProductsMock;
    PrintProducts printProductsMock;
    ImportProducts importProductsMock;

    @Before
    public void before() {
        sortProductsMock = mock(SortProducts.class);
        printProductsMock = mock(PrintProducts.class);
        importProductsMock = mock(ImportProducts.class);
    }

    @Test
    public void whenApplicationRunsThenImportProductsActionIsCalledWithCorrectPaths()
    {
        String[] files = {"file1", "file2"};
        new SortProductsApplication(sortProductsMock, printProductsMock, importProductsMock).run(files);

        verify(importProductsMock).importFromCsv(files);
    }

    @Test
    public void whenApplicationRunsThenSortProductsActionIsCalled()
    {

        List<Product> expectedProducts = new ArrayList();
        expectedProducts.add(new Product(new BigDecimal(10), "1/1/2008", 1, ""));
        expectedProducts.add(new Product(new BigDecimal(5.3), "1/1/2008", 2, ""));
        when(importProductsMock.importFromCsv(new String[]{})).thenReturn(expectedProducts);

        new SortProductsApplication(sortProductsMock, printProductsMock, importProductsMock).run(new String[]{});

        verify(sortProductsMock).sort(expectedProducts);

    }

    @Test
    public void whenApplicationRunsThenPrintProductsActionIsCalled()
    {
        List<Product> expectedProducts = new ArrayList();
        expectedProducts.add(new Product(new BigDecimal(10), "1/1/2008", 1, ""));
        expectedProducts.add(new Product(new BigDecimal(5.3), "1/1/2008", 2, ""));
        when(importProductsMock.importFromCsv(new String[]{})).thenReturn(expectedProducts);

        new SortProductsApplication(sortProductsMock, printProductsMock, importProductsMock).run(new String[]{});

        int amountToPrint = 10;
        verify(printProductsMock).print(amountToPrint, expectedProducts);
    }

}
