package com.swissquant.product.domain;

import java.math.BigDecimal;

public class Product implements Comparable<Product>{

    private String date;
    private BigDecimal value;
    private int quantity;
    private String description;

    public Product(BigDecimal value, String date, int quantity, String description) {
        this.value = value;
        this.quantity = quantity;
        this.date = date;
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public int compareTo(Product product) {
        return product.getValue().compareTo(this.value);
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String toString() {
        return "" + this.description + ", Quantity: '" + this.quantity + "', Value: '" + this.value  + "'";
    }
}
