package com.swissquant.product.actions;

import com.swissquant.product.domain.Product;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SortProductsTest
{

    @Test
    public void whenSortsProductsThenTheyAreRetrievedSortedByValue()
    {
        List<Product> products = new ArrayList();
        products.add(new Product(new BigDecimal(10), "1/1/2008", 1, ""));
        products.add(new Product(new BigDecimal(5.3), "1/1/2008", 2, ""));
        products.add(new Product(new BigDecimal(15.1), "1/1/2008", 3, ""));

        new SortProducts().sort(products);

        Assert.assertEquals(3, products.size());
        Assert.assertEquals(new BigDecimal(15.1), products.get(0).getValue());
        Assert.assertEquals(new BigDecimal(10), products.get(1).getValue());
        Assert.assertEquals(new BigDecimal(5.3), products.get(2).getValue());
    }

}
