package com.swissquant.product.actions;

import com.swissquant.product.domain.Product;
import com.swissquant.product.domain.ProductFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ImportProducts {

    public List<Product> importFromCsv(String[] files) {
        List<Product> products = new ArrayList<Product>();
        for (String file : files) {
            readFile(file, products);
        }
        return products;
    }

    private void readFile(String file, List<Product> products) {
        String csvFile = file;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] product = line.split(cvsSplitBy);
                Product createdProduct = new ProductFactory().createProduct(new BigDecimal(product[3]), product[0], new Integer(product[1]), product[2], product[4]);
                products.add(createdProduct);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
